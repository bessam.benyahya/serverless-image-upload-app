import { useState } from "react";
import ImageUploader from "react-images-upload";
import Axios from "axios";

import "./App.css";

const UploadComponent = (props) => (
  <form>
    <label>
      File upload url:
      <input
        type="text"
        id="urlInput"
        onChange={props.onUrlChange}
        value={props.url}
      />
    </label>
    <ImageUploader
      key="image-uploader"
      withIcon={true}
      singleImage={true}
      withPreview={true}
      label="Maximum size file: 5MB"
      buttonText="Choose an image to upload"
      onChange={props.onImage}
      imgExtension={[".jpg", ".png", ".jpeg"]}
      maxFileSize={5242880}
    />
  </form>
);

const App = () => {
  const [progress, setProgress] = useState("getUpload");
  const [imageUrl, setImageUrl] = useState(undefined);
  const [errorMessage, setErrorMessage] = useState("");

  const onUrlChangeHandler = (e) => {
    e.preventDefault();
    setImageUrl(e.target.value);
  };

  const onImageHandler = async (failedImages, successImages) => {
    if (!imageUrl) {
      console.log("missing imageUrl");
      setErrorMessage("missing a url to upload to");
      setProgress("uploadError");
      return;
    }

    setProgress("uploading");

    try {
      console.log("successImages", successImages);
      const parts = successImages[0].split(";");
      const mime = parts[0].split(":")[1];
      // const name = parts[1].split("=")[1];
      const image = parts[2];

      const res = await Axios.post(imageUrl, { mime, image });
      setImageUrl(res.data.imageURL);
      setProgress("uploaded");
    } catch (error) {
      console.log("error", error);
      setErrorMessage(error.message);
      setProgress("uploadError");
    }
  };

  const content = () => {
    switch (progress) {
      case "getUpload":
        return (
          <UploadComponent
            onUrlChange={onUrlChangeHandler}
            onImage={onImageHandler}
            url={imageUrl}
          />
        );
      case "uploading":
        return <div>uploading...</div>;
      case "uploaded":
        return <img src={imageUrl} alt="uploaded" />;
      case "uploadError":
        return (
          <>
            <div>Error message = {errorMessage}</div>
            <UploadComponent
              onUrlChange={onUrlChangeHandler}
              onImage={onImageHandler}
              url={imageUrl}
            />
          </>
        );
      default:
        return <div>Im default return case</div>;
    }
  };

  return (
    <div className="App">
      <h1>Image upload website</h1>
      {content()}
    </div>
  );
};

export default App;
